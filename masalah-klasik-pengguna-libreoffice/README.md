# Penggunaan Gambar pada Artikel 'Cara menggunakan LibreOffice dengan "Benar"'
Folder ini merupakan folder yang menyimpan berkas-berkas gambar yang akan di gunakan untuk Artikel 'Cara menggunakan LibreOffice dengan "Benar"' yang belum di terbitkan.

## Lisensi
[Logo LibreOffice](https://wiki.documentfoundation.org/Gallery_Logos) ([LibreOffice-External-Logo.png](https://gitlab.com/FarrelF/blog-images/blob/master/cara-menggunakan-libreoffice/LibreOffice-Logo.png) dan [LibreOffice-External-Logo.svg](https://gitlab.com/FarrelF/blog-images/blob/master/cara-menggunakan-libreoffice/LibreOffice-Logo.svg)) di buat oleh ['The Document Foundation'](https://www.documentfoundation.org/) dan (mungkin) kontributor nya, serta di lisensikan dengan sebagai berikut: 

- [Creative Commons Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)](https://creativecommons.org/licenses/by-sa/3.0/).

- [GNU Lesser General Public License versi 3 atau lebih baru (LGPL 3+)](http://www.gnu.org/licenses/lgpl.html).

- [Mozilla Public License versi 1.1 (MPL 1.1)](http://www.mozilla.org/MPL/MPL-1.1.html).

Logo tersebut juga merupakan Merek Dagang/Pakaian Dagang terdaftar dari 'The Document Foundation'. Jadi, logo-logo tersebut tetap milik mereka yang memiliki merek/pakaian dagang. Jika Anda ingin menggunakan logo-logo tersebut, silahkan kunjungi [Panduan mengenai Penggunaan Logo/Branding LibreOffice](https://wiki.documentfoundation.org/Marketing/Branding).

Selain logo, semua Gambar yang berada di dalam Folder ini, itu di buat oleh saya sendiri (Farrel Franqois) dan di lisensi kan dengan [Creative Commons Attribution-ShareAlike 4.0 (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/).
