# Farrel Franqois Blog Images
--------------------------------
Ini merupakan tempat penyimpanan berkas-berkas gambar yang akan di tampilkan di dalam [blog ini](https://farrel.franqois.id) melalui CDN.

## Apa ini?
Seperti yang telah saya jelaskan sebelumnya, *Repository* ini hanya berguna untuk menyimpan berkas gambar saja dan tidak pernah menyimpan berkas statik lain nya.

Berkas gambar yang di simpan itu akan di tampilkan di dalam blog saya (Farrel Franqois Blog) dengan CDN yang di desain khusus untuk menghantarkan berkas yang berada di dalam *Repository* Git.

## Kenapa GitLab? Dan, kenapa gak di GitHub?
Karena di GitHub hanya memberikan Penyimpanan sebesar 1 GB saja, meskipun bisa [lebih dari itu](https://help.github.com/en/articles/what-is-my-disk-quota) sampai mencapai Hard Limit (100 GB), kalo lebih dari 1 GB paling di kasih tahu kalau sebaik nya di kecilkan ukuran *Repository* nya. 

Sedangkan, GitLab memberikan batasan sebesar 10 GB untuk setiap satu *Repository*, dan itupun saya tidak mengetahui apakah itu benar-benar di berlakukan atau tidak nya, ataupun berapa Hard Limit nya, karena tidak ada referensi lebih lanjut selain jawaban di salah satu [thread forum](https://forum.gitlab.com/t/max-size-per-repo/17403) dan [blog](https://about.gitlab.com/2015/04/08/gitlab-dot-com-storage-limit-raised-to-10gb-per-repo/) [nya](https://about.gitlab.com/2017/04/11/introducing-subscriptions-on-gitlab-dot-com/) saja, dan di halaman [Biaya Paket (*Pricing*)](https://about.gitlab.com/pricing/) juga tidak di sebutkan secara eksplisit berapa batasan penyimpanan yang sebenarnya. 

Bahkan, ada [referensi](https://about.gitlab.com/2017/04/11/introducing-subscriptions-on-gitlab-dot-com/) yang menyatakan bahwa ada Batas Penyimpanan tertentu saat ini yang sebenarnya *Unmetered* (Tidak Terukur) dan batasan 10 GB per Repo belum di tegakkan secara aktif, entahlah, saya juga tidak tahu apa yang terjadi sebenarnya, mungkin saya ketinggalan berita, soalnya itu merupakan berita lama.

Nanti akan saya coba terlebih dahulu GitLab ini, nanti kalo bisa, saya *mirror*-kan *Repository* ini ke dalam GitHub kapan-kapan, sebagai percobaan saja.

## Lisensi
Karena Gambar yang ada disini memiliki Lisensi yang berbeda dan tidak ada satupun kode sumber yang tersimpan disini (Kecuali README.md), maka Lisensi nya tidak akan saya pasang disini, melainkan di dalam setiap Artikel/Laman.
