# Penggunaan Gambar pada Artikel 'Cara Install Git'
Folder ini merupakan folder yang menyimpan berkas-berkas gambar yang akan di gunakan untuk Artikel ['Cara Install Git di Windows, GNU/Linux dan macOS (tanpa basa-basi!)'](https://farrel.franqois.id/cara-install-git)

## Atribusi/Lisensi
Berkas Gambar ['Git-Logo-2Color.png'](https://gitlab.com/FarrelF/blog-images/blob/master/cara-install-git/Git-Logo-2Color.png) merupakan salah satu dari Gambar/Desain Logo Git, yang telah di buat oleh [Jason Long](https://twitter.com/jasonlong).

Gambar tersebut di lisensi kan dengan [Creative Commons Attribution 3.0 Unported (CC BY 3.0)](https://creativecommons.org/licenses/by/3.0/) oleh pembuat nya, begitupun juga dengan varian logo Git [lain nya](https://git-scm.com/downloads/logos).

Jika Anda ingin meng-unduh lebih banyak, silahkan Anda kunjungi [halaman web resmi nya](https://git-scm.com/downloads/logos).

Dan, berkas gambar ['Git-Logo-2Color-WhiteBackground.png'](https://gitlab.com/FarrelF/blog-images/blob/master/cara-install-git/Git-Logo-2Color-WhiteBackground.png) merupakan hasil modifikasi dari berkas ['Git-Logo-2Color.jpg'](https://gitlab.com/FarrelF/blog-images/blob/master/cara-install-git/Git-Logo-2Color.png) yang hanya di berikan Latar Belakang bewarna Putih saja, di Lisensi kan dengan [Creative Commons Attribution 3.0 Unported (CC BY 3.0)](https://creativecommons.org/licenses/by/3.0/).

Tapi, tetap saja, logo nya di buat oleh [Jason Long](https://twitter.com/jasonlong), saya hanya memberikan latar belakang nya saja.

Semua gambar selain berkas `Git-Logo-2Color.png` dan `Git-Logo-2Color-WhiteBackground.png` itu di buat oleh saya sendiri (Farrel Franqois), dan di lisensi kan dengan [Creative Commons Attribution-ShareAlike 4.0 (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/). Silahkan lihat berkas `LICENSE` untuk membaca Lisensi nya.
