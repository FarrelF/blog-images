# Penggunaan Gambar pada Artikel 'Cara Install Python'
Folder ini merupakan folder yang menyimpan berkas-berkas gambar yang akan di gunakan untuk Artikel 'Cara Install Python' yang belum di terbitkan.

## Lisensi
[Logo Python](https://www.python.org/community/logos/) ([python-logo.jpg](https://gitlab.com/FarrelF/blog-images/blob/master/cara-install-python/python-logo.jpg)) merupakan Merek/Pakaian Dagang dari 'Python Software Foundation' (PSF), sehingga logo tersebut merupakan logo milik mereka dan di lisensi kan oleh mereka sendiri.

Selain logo, semua Gambar yang berada di dalam Folder ini, itu di buat oleh saya sendiri (Farrel Franqois) dan di lisensi kan dengan [Creative Commons Attribution-ShareAlike 4.0 (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/).
