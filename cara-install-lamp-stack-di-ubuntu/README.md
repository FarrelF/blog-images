# Penggunaan Gambar pada Artikel 'Cara Install LAMP Stack (Apache2, MariaDB, PHP 7) + phpMyAdmin di Ubuntu dan Turunan nya'
Folder ini merupakan folder yang menyimpan berkas-berkas gambar yang akan di gunakan untuk Artikel 'Cara Install LAMP Stack (Apache2, MariaDB, PHP 7) + phpMyAdmin di Ubuntu dan Turunan nya' yang belum di terbitkan.

# Lisensi
Semua Gambar yang berada di dalam Folder ini, itu di buat oleh saya sendiri (Farrel Franqois) dan di lisensi kan dengan [Creative Commons Attribution-ShareAlike 4.0 (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/). Silahkan lihat berkas `LICENSE` untuk membaca Lisensi nya.
